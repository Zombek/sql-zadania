--1.1 Wy�wietli� zawarto�� wszystkich kolumn z tabeli pracownik.
SELECT * FROM pracownik

--1.2 Z tabeli pracownik wy�wietli� same imiona pracownik�w. 
SELECT imie FROM pracownik

--1.3 Wy�wietli� zawarto�� kolumn imi�, nazwisko i dzia� z tabeli pracownik.

SELECT imie, nazwisko, dzial FROM pracownik

--2.1 Wy�wietli� zawarto�� kolumn imi�, nazwisko i pensja z tabeli pracownik. Wynik posortuj malej�co wzgl�dem pensji.
SELECT imie, nazwisko, pensja FROM pracownik
ORDER BY pensja DESC

--2.2 Wy�wietl zawarto�� kolumn nazwisko i imi� z tabeli pracownik. Wynik posortuj rosn�co (leksykograficznie) wzgl�dem nazwiska i imienia.
SELECT nazwisko, imie FROM pracownik
ORDER BY nazwisko, imie ASC

--2.3 Wy�wietli� zawarto�� kolumn nazwisko, dzia�, stanowisko z tabeli pracownik. Wynik posortuj rosn�co wzgl�dem dzia�u, a dla tych samych nazw dzia��w malej�co wzgl�dem stanowiska. 
SELECT nazwisko, dzial, stanowisko FROM pracownik
ORDER BY dzial ASC, stanowisko DESC

--3.1 Wy�wietli� niepowtarzaj�ce si� warto�ci kolumny dzia� z tabeli pracownik. 
SELECT DISTINCT dzial FROM pracownik

--3.2 Wy�wietli� unikatowe wiersze zawieraj�ce warto�ci kolumn dzia� i stanowisko w tabeli pracownik. 
SELECT DISTINCT dzial, stanowisko FROM pracownik

--3.3 Wy�wietli� unikatowe wiersze zawieraj�ce warto�ci kolumn dzia� i stanowisko w tabeli pracownik. Wynik posortuj malej�co wzgl�dem dzia�u i stanowiska.
SELECT DISTINCT dzial, stanowisko FROM pracownik
ORDER BY dzial, stanowisko DESC

--4.1 Znajd� pracownik�w o imieniu Jan.  Wy�wietl ich imiona i nazwiska. 
SELECT imie, nazwisko FROM pracownik
WHERE imie = 'jan'

--4.2 Wy�wietli� imiona i nazwiska pracownik�w pracuj�cych na stanowisku sprzedawca
SELECT imie, nazwisko FROM pracownik
WHERE stanowisko = 'sprzedawca'

--4.3 Wy�wietli� imiona, nazwiska, pensje pracownik�w, kt�rzy zarabiaj� powy�ej 1500 z�. Wynik posortuj malej�co wzgl�dem pensji.
SELECT imie, nazwisko, pensja FROM pracownik
WHERE pensja > 1500

--5.1 Z tabeli pracownik wy�wietli� imiona, nazwiska, dzia�y, stanowiska tych pracownik�w, kt�rzy pracuj� w dziale obs�ugi klienta na stanowisku sprzedawca. 
SELECT imie, nazwisko, dzial, stanowisko FROM pracownik
WHERE dzial = 'obs�uga klienta' AND stanowisko = 'sprzedawca'

--5.2 Znale�� pracownik�w pracuj�cych w dziale technicznym na stanowisku kierownika lub sprzedawcy. Wy�wietl imi�, nazwisko, dzia�, stanowisko.
SELECT imie, nazwisko, dzial, stanowisko FROM pracownik
WHERE dzial = 'techniczny' AND (stanowisko = 'sprzedawca' OR stanowisko = 'kierownik')

--5.3 Znale�� samochody, kt�re nie s� marek fiat i ford.
SELECT * FROM samochod
WHERE NOT marka = 'fiat' AND NOT marka ='ford' 

--6.1 Znale�� samochody marek mercedes, seat i opel. 
SELECT * FROM samochod
WHERE marka IN ('mercedes', 'seat' , 'opel')

--6.2 Znajd� pracownik�w o imionach Anna, Marzena i Alicja. Wy�wietl ich imiona, nazwiska i daty zatrudnienia. 
SELECT imie, nazwisko, data_zatr FROM pracownik
WHERE imie IN ('Anna','Marzena','Alicja')

--6.3 Znajd� klient�w, kt�rzy nie mieszkaj� w Warszawie lub we Wroc�awiu. Wy�wietl ich imiona, nazwiska i miasta zamieszkania.
SELECT imie, nazwisko, miasto FROM klient
WHERE miasto IN ('Warszawa','Wroc�aw')

--7.1 Wy�wietli� imiona i nazwiska klient�w, kt�rych nazwisko zawiera liter� K.  
SELECT imie, nazwisko FROM klient
WHERE imie LIKE '%K%'

--7.2 Wy�wietli� imiona i nazwiska klient�w, dla kt�rych nazwisko zaczyna si� na D, a ko�czy si� na SKI.
SELECT imie, nazwisko FROM klient
WHERE nazwisko LIKE 'D%SKI'

--7.3 Znale�� imiona i nazwiska klient�w, kt�rych nazwisko zawiera drug� liter� O lub A. 
SELECT imie, nazwisko FROM klient
WHERE nazwisko LIKE '_O%' OR nazwisko LIKE '_A%'

--8.1 Z tabeli samoch�d wy�wietli� wiersze, dla kt�rych pojemno�� silnika jest z przedzia�u [1100,1600].
SELECT * FROM samochod
WHERE poj_silnika BETWEEN 1100 AND 1600

--8.2 Znale�� pracownik�w, kt�rzy zostali zatrudnieni pomi�dzy datami 1997-01-01 a 1997-12-31.
SELECT * FROM pracownik
WHERE data_zatr BETWEEN '1997-01-01' AND '1997-12-31' 

--8.3 Znale�� samochody, dla kt�rych przebieg jest pomi�dzy 10000 a 20000 km lub pomi�dzy 30000 a 40000 km.
SELECT * FROM samochod
WHERE przebieg BETWEEN 10000 AND 20000
OR przebieg BETWEEN 30000 AND 40000

--9.1 Znale�� pracownik�w, kt�rzy nie maj� okre�lonego dodatku do pensji.
SELECT * FROM pracownik
WHERE dodatek IS NULL

--9.2 Wy�wietli� klient�w, kt�rzy posiadaj� kart� kredytow�. 
SELECT * FROM klient
WHERE nr_karty_kredyt IS NOT NULL

--9.3 Dla ka�dego pracownika wy�wietl imi�, nazwisko i wysoko�� dodatku. Warto�� NULL z kolumny dodatek powinna by� wy�wietlona jako 0.  Wskaz�wka: U�yj funkcji COALESCE. 
SELECT imie, nazwisko, COALESCE(dodatek,0)  FROM pracownik

--10.1 Wy�wietli� imiona, nazwiska pracownik�w ich pensje i dodatki oraz kolumn� wyliczeniow� do_zap�aty, zawieraj�c� sum� pensji i dodatku. Wskaz�wka: Warto�� NULL z kolumny dodatek powinna by� wy�wietlona jako zero.  
SELECT imie, nazwisko, pensja, COALESCE(dodatek,0), pensja + COALESCE(dodatek,0) AS  do_zaplaty  FROM pracownik

--10.2 Dla ka�dego pracownika wy�wietl imi�, nazwisko i wyliczeniow� kolumn� nowa_pensja, kt�ra b�dzie mia�a o 50% wi�ksz� warto�� ni� dotychczasowa pensja. 
SELECT imie, nazwisko, (pensja + pensja /2) AS nowa_pensja  FROM pracownik

--10.3 Dla ka�dego pracownika oblicz ile wynosi 1% zarobk�w (pensja + dodatek). Wy�wietl imi�, nazwisko i obliczony 1%. Wyniki posortuj rosn�co wzgl�dem obliczonego 1%
SELECT imie, nazwisko, (pensja + COALESCE(dodatek, 0))/ 100 AS procent  FROM pracownik
ORDER BY procent ASC

--11.1 Znajd� imi� i nazwisko pracownika, kt�ry jako pierwszy zosta� zatrudniony w wypo�yczalni samochod�w.  (Jest tylko jeden taki pracownik.) 
SELECT TOP 1 imie, nazwisko FROM pracownik
ORDER BY data_zatr

--11.2 Wy�wietl pierwszych czterech pracownik�w z alfabetycznej listy (nazwiska i imiona) wszystkich pracownik�w. (W tym zadaniu nie musisz si� przejmowa� powt�rkami imion i nazwisk, ale gdyby� chcia� to sprawd� konstrukcj�  SELECT TOP x WITH TIES ...)
SELECT TOP 4 WITH TIES nazwisko, imie FROM pracownik
ORDER BY nazwisko, imie

SELECT TOP 4 * FROM pracownik
ORDER BY nazwisko, imie

--11.3 Wyszukaj informacj� o ostatnim wypo�yczeniu samochodu
SELECT TOP 1 * FROM wypozyczenie
ORDER BY data_wyp DESC

--12.1 Wyszukaj pracownik�w zatrudnionych w maju. Wy�wietl ich imiona, nazwiska i dat� zatrudnienia. Wynik posortuj rosn�co wzgl�dem nazwiska i imienia.
SELECT imie, nazwisko, data_zatr FROM pracownik
WHERE MONTH(data_zatr) = 5
ORDER BY nazwisko, imie

--12.2 Dla ka�dego pracownika (imi� i nazwisko) oblicz ile ju� pracuje dni. Wynik posortuj malej�co wed�ug ilo�ci przepracowanych dni. 
SELECT imie, nazwisko, DATEDIFF(day , data_zatr, '2019/08/25') AS przepracowane FROM pracownik
ORDER BY przepracowane DESC

--12.3 Dla ka�dego samochodu (marka, typ) oblicz ile lat up�yn�o od jego produkcji. Wynik posortuj malej�co po ilo�ci lat
SELECT marka, typ, DATEDIFF(year, data_prod,'2019/08/25') AS od_produkcji FROM samochod
ORDER BY od_produkcji DESC

--13.1 Wy�wietl imi�, nazwisko i inicja�y ka�dego klienta. Wynik posortuj wzgl�dem inicja��w, nazwiska i imienia klienta.
SELECT imie, nazwisko, CONCAT(LEFT(imie,1),'.',' ', LEFT(nazwisko,1),'.') AS inicjaly FROM klient
ORDER BY inicjaly, nazwisko, imie

--13.2 Wy�wietl imiona i nazwiska klient�w w taki spos�b, aby pierwsza litera imienia i nazwiska by�a wielka, a pozosta�e ma�e.
SELECT CONCAT(UPPER(LEFT(imie,1)),LOWER(SUBSTRING(imie,2,LEN(imie)))) AS imie,
CONCAT(UPPER(LEFT(nazwisko,1)),LOWER(SUBSTRING(nazwisko,2,LEN(nazwisko)))) AS nazwisko
FROM klient

--13.3 Wy�wietl imiona, nazwiska i numery kart kredytowych klient�w. Ka�da z ostatnich sze�ciu cyfr wy�wietlanego numeru karty kredytowej klienta powinna by� zast�piona znakiem x.
SELECT imie, nazwisko, nr_karty_kredyt FROM klient

SELECT imie, nazwisko, CONCAT(LEFT(nr_karty_kredyt,4), REPLACE(RIGHT(nr_karty_kredyt,6),RIGHT(nr_karty_kredyt,6), 'XXXXXX'))
FROM klient

--14.1 Pracownikom, kt�rzy nie maj� okre�lonej wysoko�ci dodatku nadaj dodatek w wysoko�ci 50 z�. 
UPDATE pracownik
SET dodatek = 50
WHERE dodatek IS NULL

--14.2 Klientowi o identyfikatorze r�wnym 10 zmie� imi� i nazwisko na Jerzy Nowak.
UPDATE klient
SET imie = 'Jerzy',
nazwisko = 'NOWAK'
WHERE id_klient = 10
 
--14.3 Zwi�ksz o 100 z� dodatek pracownikom, kt�rych pensja jest mniejsza ni� 1500 z�
UPDATE pracownik
SET dodatek = dodatek + 100
WHERE pensja < 1500

--15.1 Usun�� klienta o identyfikatorze r�wnym  17. 
DELETE FROM klient
WHERE id_klient = 17

--15.2 Usun�� wszystkie  informacje o wypo�yczeniach dla klienta o identyfikatorze r�wnym 17.
DELETE FROM wypozyczenie
WHERE id_klient = 17

--15.3 Usu� wszystkie samochody o przebiegu wi�kszym ni� 60000.
DELETE FROM samochod
WHERE przebieg > 60000

--16.1 Dodaj do bazy danych klienta o identyfikatorze r�wnym 121: Adam Cichy zamieszka�y ul. Korzenna 12, 00-950 Warszawa, tel. 123-454-321. 
INSERT INTO klient
VALUES (121,'Adam', 'Cichy', NULL, NULL, 'Korzenna', '12', 'Warszawa', '00-950', NULL, '123-454-321')

--16.2 Dodaj do bazy danych nowy samoch�d o identyfikatorze r�wnym 50: srebrna skoda octavia o pojemno�ci silnika 1896 cm3 wyprodukowana 1 wrze�nia 2012 r. i o przebiegu 5 000 km.
INSERT INTO  samochod
VALUES (50, 'Skoda', 'Octavia','2012-09-01','srebrny',1896,5000)

--16.3 Dodaj do bazy danych pracownika: Alojzy Mikos zatrudniony od 11 sierpnia 2010 r. w dziale zaopatrzenie na stanowisku magazyniera z pensj� 3000 z� 
--i dodatkiem 50 z�, telefon do pracownika: 501-501-501, pracownik pracuje w Warszawie na ul. Lewartowskiego 12.
INSERT INTO pracownik
VALUES(12 ,'Alojzy', 'Mikos', '2010-08-11','zaopatrzenia', 'magazynier', 3000, 50, 1, '501-501-501')