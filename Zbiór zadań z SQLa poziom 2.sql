--17.1 Wyszukaj samochody, kt�ry nie zosta�y zwr�cone. (Data oddania samochodu ma mie� warto�� NULL.) Wy�wietl identyfikator, mark� i typ samochodu 
--oraz jego dat� wypo�yczenia i oddania.

SELECT s.id_samochod, s.marka, s.typ, w.data_wyp, w.data_odd FROM samochod s
JOIN wypozyczenie w ON w.id_samochod = s.id_samochod
WHERE w.data_odd IS NULL

-- 17.2 Wyszukaj klient�w, kt�rzy nie zwr�cili jeszcze samochodu.  (Data oddania samochodu ma mie� warto�� NULL.) 
--Wy�wietl imi� i nazwisko klienta oraz identyfikator  samochodu i dat� wypo�yczenia nie zwr�conego jeszcze samochodu. 
--Wynik posortuj rosn�co wzgl�dem nazwiska i imienia klienta.  

SELECT k.imie, k.nazwisko, w.id_samochod, w.data_wyp FROM klient k
JOIN wypozyczenie w ON w.id_klient = k.id_klient
WHERE w.data_odd IS NULL

--17.3 W�r�d klient�w wypo�yczalni wyszukaj daty i kwoty wp�aconych przez nich kaucji. 
--Wy�wietl imi� i nazwisko klienta oraz dat� wp�acenia kaucji (data wypo�yczenia samochodu jest r�wnocze�nie dat� wp�acenia kaucji)
-- i jej wysoko�� (pomi� kaucje maj�ce warto�� NULL).

SELECT k.imie, k.nazwisko, w.data_wyp, w.kaucja FROM klient k
JOIN wypozyczenie w ON w.id_klient = k.id_klient
WHERE w.kaucja IS NOT NULL

--18.1 Dla ka�dego klienta, kt�ry cho� raz wypo�yczy� samoch�d, wyszukaj jakie i kiedy wypo�yczy� samochody. Wy�wietl imi� i nazwisko klienta 
--oraz dat� wypo�yczenia, mark� i typ wypo�yczonego samochodu. Wynik posortuj rosn�co po nazwisku i imieniu klienta oraz marce i typie samochodu. 
SELECT k.imie, k.nazwisko, w.data_wyp, s.marka, s.typ FROM klient k
JOIN wypozyczenie w ON w.id_klient = k.id_klient
JOIN samochod s ON s.id_samochod = w.id_samochod
ORDER BY k.nazwisko, k.imie, s.marka, s.typ

--18.2 Dla ka�dej filii wypo�yczalni samochod�w (tabela miejsce) wyszukaj jakie samochody by�y wypo�yczane. Wy�wietl  adres filii ( miasto ulica i numer) 
--oraz mark� i typ wypo�yczonego samochodu. Wyniki posortuj rosn�co wzgl�dem adresu filii, marki i typu samochodu. 

SELECT DISTINCT m.miasto, m.ulica, m.numer, s.marka, s.typ FROM miejsce m
JOIN wypozyczenie w ON w.id_miejsca_wyp = m.id_miejsce
JOIN samochod s ON s.id_samochod = w.id_samochod
ORDER BY m.miasto, m.ulica, m.numer, s.marka, s.typ

--18.3 Dla ka�dego wypo�yczonego samochodu wyszukaj informacj� jacy klienci go wypo�yczali. Wy�wietl identyfikator, mark� i typ samochodu 
--oraz imi� i nazwisko klienta. Wyniki posortuj rosn�co po identyfikatorze samochodu oraz nazwisku i imieniu klienta.

SELECT s.id_samochod, s.marka, s.typ,  k.imie, k.nazwisko FROM klient k
JOIN wypozyczenie w ON w.id_klient = k.id_klient
JOIN samochod s ON s.id_samochod = w.id_samochod
ORDER BY s.id_samochod, k.nazwisko, k.imie

--19.1 Znale�� najwi�ksz� pensj� pracownika wypo�yczalni samochod�w. 
SELECT MAX(pensja) FROM pracownik

--19.2 Znale�� �redni� pensj�  pracownika wypo�yczalni samochod�w.
SELECT AVG(pensja) FROM pracownik

--19.3 Znale�� najwcze�niejsz� dat� wyprodukowania samochodu.
SELECT MIN(data_prod) FROM samochod

--20.1 Dla ka�dego klienta wypisz imi�, nazwisko i ��czn� ilo�� wypo�ycze� samochod�w (nie zapomnij o zerowej liczbie wypo�ycze�). 
--Wynik posortuj malej�co wzgl�dem ilo�ci wypo�ycze�.  
SELECT k.imie, k.nazwisko, COUNT(w.id_klient) AS ilosc_wyp FROM klient k
LEFT JOIN wypozyczenie w ON w.id_klient = k.id_klient
GROUP BY k.imie, k.nazwisko
ORDER BY ilosc_wyp DESC

--20.2 Dla ka�dego samochodu (identyfikator, marka, typ) oblicz ilo�� wypo�ycze�. Wynik posortuj rosn�co wzgl�dem ilo�ci wypo�ycze�. 
--(Nie zapomnij o samochodach, kt�re ani razu nie zosta�y wypo�yczone.)
SELECT s.id_samochod, s.marka, s.typ, COUNT(s.id_samochod) AS ilosc_wyp FROM samochod s
LEFT JOIN wypozyczenie w ON w.id_samochod = s.id_samochod
GROUP BY s.id_samochod, s.marka, s.typ
ORDER BY ilosc_wyp

--20.3 Dla ka�dego pracownika oblicz ile wypo�yczy� samochod�w klientom. Wy�wietl imi� i nazwisko pracownika oraz ilo�� wypo�ycze�.
--Wynik posortuj malej�co po ilo�ci wypo�ycze�. (Nie zapomnij o pracownikach, kt�rzy nie wypo�yczyli �adnego samochodu.)
SELECT p.imie, p.nazwisko, COUNT(w.id_pracow_wyp) AS ilosc_wyp FROM pracownik p
LEFT JOIN wypozyczenie w ON w.id_pracow_wyp = p.id_pracownik
GROUP BY p.imie, p.nazwisko
ORDER BY ilosc_wyp DESC

--21.1 Znajd� klient�w, kt�rzy co najmniej 2 razy wypo�yczyli samoch�d. Wypisz dla tych klient�w imi�, nazwisko i ilo�� wypo�ycze�. 
--Wynik posortuj rosn�co wzgl�dem nazwiska i imienia.
SELECT k.imie, k.nazwisko, COUNT(w.id_klient) AS ilosc_wyp FROM klient k
JOIN wypozyczenie w ON w.id_klient = k.id_klient
GROUP BY k.imie, k.nazwisko
HAVING COUNT(w.id_klient) >= 2
ORDER BY k.imie, k.nazwisko

--21.2 Znajd� samochody, kt�re by�y wypo�yczone co najmniej 5 razy. 
--Wy�wietl identyfikator samochodu, mark�, typ i ilo�� wypo�ycze�. Wynik posortuj rosn�co wzgl�dem marki i typu samochodu. 
SELECT s.id_samochod, s.marka, s.typ, COUNT(s.id_samochod) AS ilosc_wyp FROM samochod s
JOIN wypozyczenie w ON w.id_samochod = s.id_samochod
GROUP BY s.id_samochod, s.marka, s.typ
HAVING COUNT(s.id_samochod) >= 5
ORDER BY s.marka, s.typ

--21.3 Znajd� pracownik�w, kt�rzy klientom wypo�yczyli co najwy�ej 20 razy samoch�d. Wy�wietl imiona i nazwiska pracownik�w razem z ilo�ci� wypo�ycze�. 
--Wynik posortuj rosn�co wzgl�dem ilo�ci wypo�ycze�, nazwiska i imienia pracownika. (Uwzgl�dnij pracownik�w, kt�rzy nie wypo�yczyli �adnego samochodu.)
SELECT p.imie, p.nazwisko, COUNT(w.id_pracow_wyp) AS ilosc_wyp FROM pracownik p
JOIN wypozyczenie w ON w.id_pracow_wyp = p.id_pracownik
GROUP BY p.imie, p.nazwisko
HAVING COUNT(w.id_pracow_wyp) >20
ORDER BY ilosc_wyp, p.nazwisko, p.imie

--22.1 Wy�wietl imiona, nazwiska i pensje pracownik�w, kt�rzy posiadaj� najwy�sz� pensj�. 
SELECT imie, nazwisko, pensja FROM pracownik
WHERE pensja = (SELECT MAX(pensja) FROM pracownik)

--22.2 Wy�wietl pracownik�w (imiona, nazwiska, pensje), kt�rzy zarabiaj� powy�ej �redniej pensji. 
SELECT imie, nazwisko, pensja FROM pracownik
WHERE pensja > (SELECT AVG (pensja) FROM pracownik)

--22.3 Wyszukaj samoch�d (marka, typ, data produkcji), kt�ry zosta� wyprodukowany najwcze�niej. 
--Mo�e si� tak zdarzy�, �e kilka samochod�w zosta�o wyprodukowanych w ten sam "najwcze�niejszy" dzie�.
SELECT marka, typ, data_prod FROM samochod
WHERE data_prod = (SELECT MIN(data_prod ) FROM samochod)

--23.1 Wy�wietl wszystkie samochody (marka, typ, data produkcji), kt�re do tej pory nie zosta�y wypo�yczone. 
SELECT marka, typ, data_prod FROM samochod
WHERE id_samochod NOT IN (SELECT id_samochod FROM wypozyczenie)

--23.2 Wy�wietl klient�w (imi� i nazwisko), kt�rzy do tej pory nie wypo�yczyli �adnego samochodu. 
--Wynik posortuj rosn�co wzgl�dem nazwiska i imienia klienta.

SELECT imie, nazwisko FROM klient
WHERE id_klient NOT IN (SELECT id_klient FROM wypozyczenie)
ORDER BY nazwisko, imie

--23.3 Znale�� pracownik�w (imi� i nazwisko), kt�rzy do tej pory nie wypo�yczyli �adnego samochodu klientowi.
SELECT imie, nazwisko FROM pracownik
WHERE id_pracownik NOT IN (SELECT id_miejsca_wyp FROM wypozyczenie)

--24.1 Znajd� samoch�d/samochody (id_samochod, marka, typ), kt�ry by� najcz�ciej wypo�yczany. 
--Wynik posortuj rosn�co (leksykograficznie) wzgl�dem marki i typu. 
SELECT s.id_samochod, s.marka, s.typ FROM samochod s
JOIN wypozyczenie w ON w.id_samochod = s.id_samochod
GROUP BY s.id_samochod, s.marka, s.typ
HAVING COUNT(w.id_samochod) = (SELECT TOP 1 COUNT(id_samochod) as ilosc FROM wypozyczenie
								GROUP BY id_samochod
								ORDER BY ilosc DESC)
ORDER BY s.marka, s.typ 

--24.2 Znajd� klienta/klient�w (id_klient, imie, nazwisko), kt�rzy najrzadziej wypo�yczali samochody. Wynik posortuj rosn�co wzgl�dem nazwiska i imienia. 
--Nie uwzgl�dniaj klient�w, kt�rzy ani razu nie wypo�yczyli samochodu. 
SELECT k.id_klient, k.imie, k.nazwisko FROM klient k
JOIN wypozyczenie w ON w.id_klient = k.id_klient
GROUP BY  k.id_klient, k.imie, k.nazwisko 
HAVING COUNT(w.id_klient) = (SELECT TOP 1 COUNT(id_klient) as ilosc_wyp From wypozyczenie
							GROUP BY id_klient
							ORDER BY ilosc_wyp)
ORDER BY k.nazwisko , k.imie

--24.3 Znajd� pracownika/pracownik�w (id_pracownik, nazwisko, imie), kt�ry wypo�yczy� najwi�cej samochod�w klientom. 
--Wynik posortuj rosn�co (leksykograficznie) wzgl�dem nazwiska i imienia pracownika.
SELECT p.id_pracownik, p.imie, p.nazwisko FROM pracownik p
JOIN wypozyczenie w ON w.id_pracow_wyp = p.id_pracownik
GROUP BY  p.id_pracownik, p.imie, p.nazwisko
HAVING COUNT(w.id_pracow_wyp) = (SELECT TOP 1 COUNT(id_pracow_wyp) as ilosc_wyp From wypozyczenie
							GROUP BY id_pracow_wyp
							ORDER BY ilosc_wyp DESC)
ORDER BY p.nazwisko , p.imie

--25.1 Podwy�szy� o 10% pensj� pracownikom, kt�rzy zarabiaj� poni�ej �redniej. 

UPDATE pracownik
SET pensja = pensja + pensja/10 
WHERE pensja < (SELECT AVG(pensja) FROM pracownik)

--25.2 Pracownikom, kt�rzy w maju wypo�yczyli samoch�d klientowi zwi�ksz dodatek o 10 z�. 
UPDATE pracownik
SET dodatek = dodatek + 10
WHERE id_pracownik IN (SELECT id_pracow_wyp FROM wypozyczenie
						WHERE MONTH(data_wyp) = 5)

--25.3 Obni�y� pensje o 5% wszystkim pracownikom kt�rzy nie wypo�yczyli klientowi samochodu w 1999 roku.
UPDATE pracownik
SET pensja = pensja*0.95
WHERE id_pracownik NOT IN	(SELECT id_pracow_wyp FROM wypozyczenie
							WHERE YEAR(data_wyp) = 1999)

--26.1 Usun�� klient�w, kt�rzy nie wypo�yczyli �adnego samochodu. 
DELETE FROM klient
WHERE id_klient NOT IN (SELECT id_klient FROM wypozyczenie)

--26.2 Usun�� samochody, kt�re nie zosta�y ani razu wypo�yczone. 
DELETE FROM samochod
WHERE id_samochod NOT IN (SELECT id_samochod FROM wypozyczenie)

--26.3 Usun�� pracownik�w, kt�rzy klientom nie wypo�yczyli �adnego samochodu.
DELETE FROM pracownik
WHERE id_pracownik NOT IN (SELECT id_pracownik FROM wypozyczenie)

--27.1 Wy�wietl razem wszystkie imiona i nazwiska pracownik�w i klient�w. (Suma dw�ch zbior�w.) Wynik posortuj wzgl�dem nazwiska i imienia. 
--Rozpatrz dwa przypadkia) z pomini�ciem duplikat�w, b) z wy�wietleniem duplikat�w (pe�na suma). Wskaz�wka:https://msdn.microsoft.com/pl-pl/library/ff848745(v=sql.110).aspx. 
SELECT imie, nazwisko FROM klient
UNION
SELECT imie, nazwisko FROM pracownik
ORDER BY nazwisko, imie

SELECT imie, nazwisko FROM klient
UNION ALL
SELECT imie, nazwisko FROM pracownik
ORDER BY nazwisko, imie

--27.2 Wy�wietl powtarzaj�ce si� imiona i nazwiska klient�w i pracownik�w. (Cz�� wsp�lna dw�ch zbior�w.) 
SELECT imie, nazwisko FROM klient
INTERSECT
SELECT imie, nazwisko FROM pracownik
ORDER BY nazwisko, imie

--27.3 Wy�wietl imiona i nazwiska klient�w, kt�rzy nazywaj� si� inaczej ni� pracownicy. (R�nica dw�ch zbior�w.) Wynik posortuj wzgl�dem nazwiska i imienia. 
SELECT imie, nazwisko FROM klient
EXCEPT
SELECT imie, nazwisko FROM pracownik
ORDER BY nazwisko, imie

