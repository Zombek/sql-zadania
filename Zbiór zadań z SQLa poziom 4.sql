--32.1 Napisa� procedur� o nazwie wypisz_samochody, kt�ra posiada tylko jeden parametr - marka samochodu. 
--Procedura powinna wy�wietla� wszystkie informacje z tabeli samoch�d o samochodach zadanej marki. 

			--CREATE PROCEDURE wypisz_samochody @marka VARCHAR(20)
			--	AS
			--		SELECT * FROM samochod
			--		WHERE marka = @marka
			--	GO
			--EXEC wypisz_samochody 'opel'

--32.2 Napisa� procedur� o nazwie zwieksz_pensje posiadaj�c� dwa parametry: identyfikator pracownika i kwot�. 
--Procedura powinna zwi�kszy� pensj� pracownikowi, na kt�rego wskazuje zadany identyfikator o zadan� kwot�. 
--Przetestuj utworzon� procedur� � zwi�ksz pracownikowi o identyfikatorze r�wnym 1 pensj� o 1000 z�. 

			--CREATE PROCEDURE zwieksz_pensje @id INT, @kwota INT
			--	AS
			--		UPDATE pracownik
			--		SET pensja = pensja + @kwota
			--		WHERE id_pracownik = @id
			--	GO

			--SELECT * FROM pracownik

			--EXEC zwieksz_pensje 1,1000

			--SELECT * FROM pracownik

--32.3 Napisz procedur� o nazwie dodaj_klienta umo�liwiaj�c� dodanie nowego klienta. Dane klienta powinny by� odczytane z parametr�w procedury. 
--Dobierz  odpowiednio parametry dla tworzonej procedury na podstawie kolumn tabeli klient. Przetestuj utworzon� procedur� � dodaj nowego klienta.

			--CREATE PROCEDURE dodaj_klienta @id_klient INT, @imie VARCHAR(15), @nazwisko VARCHAR(20), @nr_karty_kredyt VARCHAR(20), @firma VARCHAR(40), 
			--@ulica VARCHAR(24), @numer VARCHAR(8) , @miasto VARCHAR(24), @kod VARCHAR(6), @nip VARCHAR(11), @telefon VARCHAR(16)
			--	AS
			--		INSERT INTO klient
			--		VALUES(@id_klient, @imie, @nazwisko, @nr_karty_kredyt, @firma, @ulica, @numer, @miasto, @kod, @nip, @telefon)
			--	GO

			--EXEC dodaj_klienta 150, 'Ferdek' , 'Kiepski', NULL, NULL, '�wiartki', '3\4', 'Wroc�aw', '00-200', NULL, NULL

			--SELECT * FROM klient

--33.1 Napisa�  funkcj� o nazwie aktywnosc_klienta, kt�ra b�dzie zwraca� ilo�� wypo�ycze� samochod�w dla klienta o identyfikatorze 
--zadanym jako parametr funkcji. Przetestuj utworzon� funkcj� � sprawd� ile samochod�w wypo�yczy� klient o identyfikatorze r�wnym 3. 

			--CREATE FUNCTION dbo.aktywnosc_klienta(@id INT)
			--	RETURNS INT
			--		AS BEGIN
			--			RETURN	(SELECT COUNT(id_klient) FROM wypozyczenie
			--					WHERE id_klient = @id)
			--		END
			--	GO

			--SELECT dbo.aktywnosc_klienta(3) AS ilosc

--33.2 Napisa� funkcj� o nazwie ile_wypozyczen posiadaj�c� dwa parametry data_od i data_do. 
--Funkcja powinna zwr�ci� ilo�� wypo�ycze� samochod�w w zadanym przedziale czasowym. Przetestuj utworzon� funkcj� 
--� sprawd� ile zosta�o wypo�yczonych samochod�w od 01.01.2000r. do 31.12.2000r. 

			--DROP FUNCTION dbo.ile_wypozyczen

			--CREATE FUNCTION dbo.ile_wypozyczen (@od DATETIME, @do DATETIME)
			--	RETURNS INT
			--		AS BEGIN
			--			RETURN	(SELECT COUNT(id_klient) FROM wypozyczenie
			--					WHERE data_wyp BETWEEN @od AND @do )
			--		END
			--	GO

			--SELECT dbo.ile_wypozyczen('2000-01-01', '2000-12-21') AS ilosc

--33.3 Napisa� funkcj� o nazwie roznica_pensji nie posiadaj�c� parametr�w i zwracaj�c� r�nic� pomi�dzy najwi�ksz� i 
--najmniejsz� pensj� w�r�d pracownik�w wypo�yczalni. Przetestuj utworzon� funkcj�. 

			--CREATE FUNCTION dbo.roznica_pensji()
			--	RETURNS INT
			--		AS BEGIN
			--			RETURN	(SELECT MAX(pensja) - MIN(pensja) FROM pracownik)
			--		END
			--	GO

			--SELECT dbo.roznica_pensji() AS roznica

--34.1 Utw�rz widok o nazwie klient_raport zawieraj�cy informacje o ilo�ci wypo�ycze� ka�dego z klient�w (id_klient, imie, nazwisko). 
--Uwzgl�dnij klient�w, kt�rzy ani razu nie wypo�yczyli samochodu. Za pomoc� utworzonego widoku znajd� klient�w, kt�rzy wypo�yczyli samoch�d wi�cej ni� raz.

			--CREATE VIEW klient_raport  
			--	AS 
			--		SELECT k.id_klient, k.imie, k.nazwisko, COUNT(w.id_klient) AS ilosc_wyp FROM klient k 
			--		LEFT JOIN wypozyczenie w ON k.id_klient=w.id_klient 
			--		GROUP BY k.id_klient, k.imie, k.nazwisko; 
			--	GO

			--SELECT * FROM klient_raport WHERE ilosc_wyp>1;

-- 34.2 Utw�rz widok o nazwie samochod_raport zawieraj�cy informacje o ilo�ci wypo�ycze� ka�dego z samochod�w (id_samochod, marka, typ). 
--Uwzgl�dnij samochody, kt�re ani razu nie zosta�y wypo�yczone. Za pomoc� utworzonego widoku znajd� samoch�d/samochody, kt�re by�y najcz�ciej wypo�yczane. 
			
			--CREATE VIEW samochod_raport
			--	AS
			--		SELECT s.id_samochod, s.marka, s.typ, COUNT(w.id_samochod) AS ilosc_wyp FROM wypozyczenie w
			--		LEFT JOIN samochod s ON w.id_samochod = s.id_samochod
			--		GROUP BY s.id_samochod, s.marka, s.typ
			--	GO
 
			--DROP VIEW samochod_raport

			--SELECT * FROM samochod_raport
			--WHERE ilosc_wyp = (SELECT MAX(ilosc_wyp) FROM samochod_raport)

--34.3 Utw�rz widok o nazwie pracownik_raport zawieraj�cy informacje o ilo�ci wypo�ycze� samochod�w przez pracownik�w. 
--Nie zapomnij uwzgl�dni� pracownik�w, kt�rzy nie wypo�yczyli �adnego samochodu. Za pomoc� utworzonego widoku znajd� pracownik�w, 
--dla kt�rych ilo�� wypo�ycze� jest wi�ksza od �redniej ilo�ci wypo�ycze� samochod�w przez pracownik�w.

			--CREATE VIEW pracownik_raport
			--	AS	
			--		SELECT p.id_pracownik, p.imie, p.nazwisko, COUNT(w.id_pracow_wyp) as ilosc_wyp from pracownik p
			--		LEFT JOIN wypozyczenie w ON w.id_pracow_wyp = p.id_pracownik
			--		GROUP BY p.id_pracownik, p.imie, p.nazwisko
			--	GO

			--SELECT * FROM pracownik_raport
			--WHERE ilosc_wyp > (SELECT AVG(ilosc_wyp) FROM pracownik_raport)

--35.1 Utw�rz unikalny indeks dla kolumny telefon w tabeli klient.      
		--CREATE UNIQUE INDEX index_phone
		--ON klient(telefon)             
                        
--35.2 Utw�rz indeks klastrowy dla kolumn nazwisko i imie w tabeli klient. 
		--ALTER TABLE klient
		--DROP CONSTRAINT PK__klient__C6465E77C1D98FE9

		--CREATE CLUSTERED INDEX index_imieNazwisko
		--ON klient (nazwisko, imie)

		--SELECT * FROM klient

--35.3 Utw�rz indeks nieklastrowy dla kolumn marka i typ w tabeli samoch�d.
		--CREATE NONCLUSTERED INDEX index_markaTyp
		--ON samochod(marka, typ)

--36.1 Napisa� wyzwalacz, kt�ry uniemo�liwi usuni�cie klienta.  Przetestuj utworzony wyzwalacz � spr�buj usun�� wszystkich klient�w. 
		--CREATE TRIGGER usun_klient
		--	ON klient
		--	FOR DELETE
		--AS RAISERROR('zakaz usuwania klient�w',2,1)
		--	ROLLBACK 

		--	DELETE FROM klient
		--	where id_klient = 5
		
--36.2 Napisa� wyzwalacz, kt�ry uniemo�liwi dodanie pracownika z pensj� i dodatkiem r�wnym zero lub NULL.  
--Przetestuj utworzony wyzwalacz � spr�buj doda� kilku pracownik�w jednocze�nie. 
		
		DROP TRIGGER dodaj_pracownik
	
		CREATE TRIGGER dodaj_pracownik ON pracownik
		FOR INSERT
		AS
			DECLARE @pensja DECIMAL(8,2)
			DECLARE @dodatek DECIMAL(8,2)
			SELECT @pensja = pensja FROM inserted
			SELECT @dodatek = dodatek FROM inserted
			IF @pensja <= 0 OR @pensja IS NULL OR @dodatek <= 0 OR @dodatek IS NULL
		BEGIN
		ROLLBACK
		RAISERROR('Nie mo�na wstawi� pracownika z pensj�/dodatkiem poni�ej zera lub null',1,2)
		END

		INSERT INTO pracownik
		VALUES(252,'Adam', 'Ma�ysz', GETDATE(), 'techniczny', 'mechanik', 0, 100, 1, NULL)

		SELECT * FROM pracownik

--36.3 Napisa� wyzwalacz o nazwie "duplikat_miejsce", kt�ry uniemo�liwi dodanie miejsca o takich samych ulicach, numerze, mie�cie i kodzie.

		DROP TRIGGER duplikat_miejsce
	
		CREATE TRIGGER duplikat_miejsce ON miejsce
		FOR INSERT
		AS
			DECLARE @ulica VARCHAR(24)
			DECLARE @numer VARCHAR(8)
			DECLARE @miasto VARCHAR(24)
			DECLARE @kod VARCHAR(6)

			SELECT @ulica = ulica FROM inserted
			SELECT @numer = numer FROM inserted
			SELECT @miasto = miasto FROM inserted
			SELECT @kod = kod FROM inserted
			--zlicz wyst�pienia
			DECLARE @ileUlic INT	SELECT @ileUlic = COUNT(ulica) FROM miejsce WHERE ulica = @ulica
			DECLARE @ileNumer INT	SELECT @ileNumer = COUNT(numer) FROM miejsce WHERE numer = @numer
			DECLARE @ileMiasto INT	SELECT @ileMiasto = COUNT(miasto) FROM miejsce WHERE miasto = @miasto
			DECLARE @ileKod INT		SELECT @ileKod = COUNT(kod) FROM miejsce WHERE kod = @kod

			--jak jest wi�cej ni� jedno cofnij
			IF	
				@ileUlic > 1 AND
				@ileNumer > 1 AND
				@ileMiasto > 1 AND
				@ileKod > 1 

		BEGIN
		ROLLBACK
		RAISERROR('Takie Miejsce ju� istnieje',1,2)
		END

		-- NIE PRZEJDZIE
		INSERT INTO miejsce
		VALUES (123,'Lewartowskiego', '12', 'Warszawa','10-100', NULL, NULL)
		--PRZEJDZIE
		INSERT INTO miejsce
		VALUES (1216,'Lenarta', '12A',	'Wroc�aw','20-100', NULL, NULL)

--37.1 a) Do tabeli samoch�d dodaj kolumn� "usuniety" typu BIT o warto�ci domy�lnej r�wnej 0. 
--b) W tabeli samochod zmie� wszystkie warto�ci kolumny usuniety z NULL na warto�� 0.
-- c) Utw�rz wyzwalacz o nazwie " usuniety_samochod ", kt�ry uniemo�liwi fizyczne usuni�cie samochodu z tabeli samochod,
-- a usuwany samochod oznaczy poprzez ustawienie kolumny usuniety na warto�� 1. 

ALTER TABLE samochod
ADD usuniety BIT

UPDATE samochod
SET usuniety = 0

DROP TRIGGER usuniety_samochod

	CREATE TRIGGER usuniety_samochod ON samochod
		INSTEAD OF DELETE
		AS
		BEGIN 
			DECLARE @id INT
			SELECT @id = id_samochod FROM deleted
			
			UPDATE samochod
			SET usuniety = 1
			WHERE id_samochod = @id

		END

		DELETE FROM samochod
		WHERE id_samochod IN (3)

		SELECT * FROM samochod

		

--37.2 Utw�rz wyzwalacz o nazwie "usun_miejsce_i_wypozyczenia", kt�ry uniemo�liwi usuni�cie r�wnocze�nie wi�cej ni� 1 miejsca 
--z tabeli miejsce oraz przed usuni�ciem pojedynczego miejsca usunie najpierw wszystkie rekordy w tabeli wypo�yczenie zawieraj�ce 
--informacj� o usuwanym miejscu (id_miejsca_wyp, id_miejsca_odd). Przetestuj napisany wyzwalacz. 

		DROP TRIGGER usun_miejsce_i_wypozyczenia1
	
		CREATE TRIGGER usun_miejsce_i_wypozyczenia1 ON miejsce
		FOR DELETE
		AS
			DECLARE @ile INT
			SELECT @ile = COUNT(id_miejsce) FROM deleted
			--jak jest wi�cej ni� jedno cofnij
			IF	
				@ile > 1 
			
		BEGIN
		ROLLBACK
		RAISERROR('Wolno usuwa� tylko jedno miejsce na raz',1,2)
		END
		ELSE
		BEGIN
			DECLARE @id INT		SELECT @id = id_miejsce FROM deleted
			UPDATE wypozyczenie
			SET id_miejsca_odd = NULL
			WHERE id_miejsca_odd = @id
			
			UPDATE wypozyczenie
			--KOLUMNA NIE POZWALA NA NULL, TRZEBA BY STWORZY� INNE MIEJSE LUB ALTER COLUMN
			SET id_miejsca_wyp = NULL
			WHERE id_miejsca_wyp = @id
		END

		DELETE FROM miejsce
		WHERE id_miejsce IN (2,3,4)

		DELETE FROM miejsce
		WHERE id_miejsce IN (2)

--37.3 Utw�rz wyzwalacz o nazwie "samochod_blokada", kt�ry uniemo�liwia wprowadzenie jakiejkolwiek zmiany w tabeli samoch�d. 
--Przetestuj utworzony wyzwalacz dla instrukcji INSERT, UPDATE, DELETE. 

		DROP TRIGGER samochod_blokada
	
		CREATE TRIGGER samochod_blokada ON samochod
		FOR DELETE, INSERT, UPDATE
		AS			
		BEGIN
		ROLLBACK
		RAISERROR('NIE WOLNO MODYFIKOWA� TABELI',1,2)
		END

		DELETE FROM samochod
		WHERE id_samochod = 5

		UPDATE samochod
		SET marka = 'test'
		WHERE id_samochod = 5

		INSERT INTO samochod
		VALUES(123, 'test','test',GETDATE(),'test',1600,1600,0)