--28.1 Utw�rz tabel� pracownik2(id_pracownik, imie, nazwisko, pesel, data_zatr, pensja), gdzie  
--* id_pracownik � jest numerem pracownika nadawanym automatycznie, jest to klucz g��wny 
--* imie i nazwisko to niepuste �a�cuchy znak�w zmiennej d�ugo�ci, 
--* pesel  unikatowy �a�cuch jedenastu znak�w sta�ej d�ugo�ci, 
--* data_zatr  domy�lna warto�� daty zatrudnienia to bie��ca data systemowa, 
--* pensja nie mo�e by� ni�sza ni� 1000z�. 
CREATE TABLE pracownik2(
id_pracownik INT IDENTITY PRIMARY KEY,
imie_nazwisko VARCHAR(255),
PESEL VARCHAR(11) NOT NULL UNIQUE,
data_zatr DATE DEFAULT GETDATE(),
pensja INT CHECK (pensja > 1000)
);

--28.2 Utw�rz tabel� naprawa2(id_naprawa, data_przyjecia, opis_usterki, zaliczka), gdzie 
--* id_naprawa  jest unikatowym, nadawanym automatycznie numerem naprawy, jest to klucz g��wny, 
--* data_przyjecia � nie mo�e by� p�niejsza ni� bie��ca data systemowa, 
--* opis usterki � nie mo�e by� pusty, musi mie� d�ugo�� powy�ej 10 znak�w, 
--* zaliczka � nie mo�e by� mniejsza ni� 100z� ani wi�ksza ni� 1000z�. 

CREATE TABLE naprawa2(
id_naprawa INT IDENTITY PRIMARY KEY,
data_przyjecia DATE,
opis_usterki VARCHAR(255) NOT NULL,
zaliczka INT, 
CONSTRAINT CHECK_naprawa CHECK (zaliczka > 100 AND zaliczka < 1000 AND LEN(opis_usterki) > 10 AND data_przyjecia >= GETDATE())
);

--28.3 Utw�rz tabel� wykonane_naprawy2(id_pracownik, id_naprawa, data_naprawy, opis_naprawy, cena), gdzie 
--* id_pracownik � identyfikator pracownika wykonuj�cego napraw�, klucz obcy powi�zany z tabel� pracownik2, 
--* id_naprawa � identyfikator zg�oszonej naprawy, klucz obcy powi�zany z tabel� naprawa2, 
--* data_naprawy � domy�lna warto�� daty naprawy to bie��ca data systemowa, 
--* opis_naprawy � niepusty opis informuj�cy o sposobie naprawy, 
--* cena � cena naprawy. 
CREATE TABLE wykonane_naprawy2(
id_pracownik INT FOREIGN KEY REFERENCES pracownik2(id_pracownik),
id_naprawa INT FOREIGN KEY REFERENCES naprawa2(id_naprawa),
data_naprawy DATE DEFAULT GETDATE(),
opis_naprawy VARCHAR(255) NOT NULL,
cena INT
);

--29.1 Dana jest tabela 
CREATE TABLE student2(id_student INT IDENTITY(1,1) PRIMARY KEY, nazwisko VARCHAR(20), nr_indeksu INT, stypendium MONEY);
--Wprowad� ograniczenia na tabel� student2: 
--* nazwisko � niepusta kolumna, 
--* nr_indeksu � unikatowa kolumna, 
--* stypendium �nie mo�e by� ni�sze ni� 1000z�, 
--* dodatkowo dodaj niepust� kolumn� imie.
ALTER TABLE student2
ADD CONSTRAINT CHK_student2 CHECK (nazwisko IS NOT NULL AND stypendium > 1000)
ALTER TABLE student2
ADD CONSTRAINT UC_student2 UNIQUE (nr_indeksu)
ALTER TABLE student2
ADD imie VARCHAR(255) NOT NULL

--29.2 Dane s� tabele: 
CREATE TABLE dostawca2(id_dostawca INT IDENTITY(1,1) PRIMARY KEY, nazwa VARCHAR(30)); CREATE TABLE towar2(id_towar INT IDENTITY(1,1) PRIMARY KEY, kod_kreskowy INT, id_dostawca INT); 
--Zmodyfikuj powy�sze tabele: 
--* kolumna nazwa z tabeli dostawca2 powinna by� unikatowa, 
--* do tabeli towar2 dodaj niepust� kolumn� nazwa, 
--* kolumna kod_kreskowy w tabeli towar2 powinna by� unikatowa,
--* kolumna id_dostawca z tabeli towar2 jest kluczem obcym z tabeli dostawca2.
ALTER TABLE dostawca2
ADD CONSTRAINT UC_dostawca2 UNIQUE (nazwa)

ALTER TABLE towar2
ADD nazwa VARCHAR(255) NOT NULL

ALTER TABLE towar2
ADD CONSTRAINT CHK_towar2 UNIQUE (kod_kreskowy)

ALTER TABLE towar2
ADD FOREIGN KEY (id_dostawca) REFERENCES dostawca2(id_dostawca)

--29.3 Dane s� tabele:  
CREATE TABLE kraj2(id_kraj INT IDENTITY(1,1) PRIMARY KEY, nazwa VARCHAR(30)); 
CREATE TABLE gatunek2(id_gatunek INT IDENTITY(1,1) PRIMARY KEY, nazwa VARCHAR(30));
CREATE TABLE zwierze2(id_zwierze INT IDENTITY(1,1) PRIMARY KEY, id_gatunek INT, id_kraj INT, cena MONEY); 
--Zmodyfikuj powy�sze tabele: 
--* kolumny nazwa z tabel kraj2 i gatunek2 maj� by� niepuste, 
--* kolumna id_gatunek z tabeli zwierze2 jest kluczem obcym z tabeli gatunek2, 
--* kolumna id_kraj z tabeli zwierze2 jest kluczem obcym z tabeli kraj2.

ALTER TABLE kraj2
ADD CONSTRAINT CHK_kraj2 CHECK (nazwa IS NOT NULL)

ALTER TABLE gatunek2
ADD CONSTRAINT CHK_gatunek2 CHECK (nazwa IS NOT NULL)

ALTER TABLE zwierze2
ADD FOREIGN KEY (id_gatunek) REFERENCES gatunek2(id_gatunek)

ALTER TABLE zwierze2
ADD FOREIGN KEY (id_kraj) REFERENCES kraj2(id_kraj)

--30.1 Dane s� tabele: 
CREATE TABLE kategoria2(id_kategoria INT PRIMARY KEY, nazwa VARCHAR(30) );  
CREATE TABLE przedmiot2(id_przedmiot INT PRIMARY KEY, id_kategoria INT REFERENCES kategoria2(id_kategoria), nazwa VARCHAR(30)); 
--Napisa� instrukcje SQL, kt�ra usun� tabele kategoria2 i przedmiot2. 
--Wsk: Zwr�� uwag� na kolejno�� usuwania tabel. Wersja trudniejsza: 
--Czy potrafisz najpierw sprawdzi�, czy tabele istniej� i je�li istniej� to dopiero wtedy  je usun��? 

DROP TABLE przedmiot2
DROP TABLE kategoria2

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE table_name='przedmiot2')
BEGIN
    DROP TABLE przedmiot2
END

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE table_name='kategoria2')
BEGIN
    DROP TABLE kategoria2
END

--30.2 Dana jest tabela: 
CREATE TABLE osoba2(id_osoba INT, imie VARCHAR(15), imie2 VARCHAR(15) );
-- Napisa� instrukcj� SQL, kt�ra z tabeli osoba2 usunie kolumn� imie2.
ALTER TABLE osoba2
DROP COLUMN imie2

--30.3 Dana jest tabela: 
CREATE TABLE uczen2(id_uczen INT PRIMARY KEY, imie VARCHAR(15), nazwisko VARCHAR(20) CONSTRAINT uczen_nazwisko_unique UNIQUE); 
--Napisa� instrukcj� SQL, kt�ra usunie narzucony warunek unikatowo�ci na kolumn� nazwisko.
ALTER TABLE uczen2
DROP CONSTRAINT uczen_nazwisko_unique

--31.1 Utw�rz tabel� wlasciciel2(id_wlasciciel, imie,nazwisko,data_ur,ulica,numer,kod,miejscowosc) 
--i zwierze2(id_zwierze,id_wlasciciel,rasa,data_ur,imie) w taki spos�b, 
--aby po usuni�ciu informacji o w�a�cicielu zwierz�cia z tabeli wlasciciel2, 
--SZBD automatycznie identyfikator w�a�ciciela w tabeli zwierze2 ustawia� na warto�� NULL. 
--Nie zapomnij o doborze pozosta�ych ogranicze� na kolumny (mo�na to zrobi� wg uznania). 

CREATE TABLE wlasciciel2(
id_wlasciciel INT IDENTITY PRIMARY KEY, 
imie VARCHAR(255),
nazwisko VARCHAR(255),
data_ur DATE,
ulica VARCHAR(255),
numer VARCHAR(255),
kod VARCHAR(255),
miejscowosc VARCHAR(255)
) 

CREATE TABLE zwierze3(
id_zwierze INT IDENTITY PRIMARY KEY,
id_wlasciciel INT,
rasa VARCHAR(255),
data_ur DATE,
imie VARCHAR(255)

	 CONSTRAINT OND_wlasciciel
	 FOREIGN KEY (id_wlasciciel)
	 REFERENCES wlasciciel2 (id_wlasciciel)
	 ON DELETE SET NULL
)


--31.2 Dane s� tabele: 
CREATE TABLE film2(id_film INT PRIMARY KEY,tytul VARCHAR(50) NOT NULL); 
CREATE TABLE gatunek3(id_gatunek INT PRIMARY KEY,nazwa VARCHAR(50) NOT NULL); 
CREATE TABLE film2_gatunek2(id_film INT,id_gatunek INT,PRIMARY KEY(id_film,id_gatunek)); 
--Zmodyfikuj struktur� powy�szych tabel (polecenie ALTER) taka aby: 
--a) w przypadku usuni�cia filmu z tabeli film2 zosta�y automatycznie usuwane informacje jakich gatunk�w by� usuni�ty film (nie usuwamy gatunk�w z tabeli gatunek2), 
--b) w przypadku usuni�cia gatunku z tabeli gatunek2 zosta�y automatycznie usuwane informacje 
--jakie filmy by�y tego gatunku (nie usuwamy film�w z tabeli film2).

ALTER TABLE film2_gatunek2
ADD CONSTRAINT OND_FILM
FOREIGN KEY (id_film)
REFERENCES film2(id_film)
ON DELETE CASCADE

ALTER TABLE film2_gatunek2
ADD CONSTRAINT OND_GATUNEK
FOREIGN KEY (id_gatunek)
REFERENCES gatunek3(id_gatunek)
ON DELETE SET NULL

--31.3 Dane s� tabele: 
CREATE TABLE stanowisko2 (id_stanowisko INT PRIMARY KEY, nazwa VARCHAR(30)); 
CREATE TABLE pracownik3(id_pracownik INT PRIMARY KEY, id_stanowisko INT, nazwisko VARCHAR(20));
--Zmodyfikuj struktury powy�szych tabel tak, aby po usuni�ciu stanowiska z tabeli stanowisko2 identyfikator 
--stanowiska w tabeli pracownik2 by� automatycznie ustawiany na warto�� NULL oraz podczas modyfikowania 
--identyfikatora stanowiska w tabeli stanowisko2 wszystkie identyfikatory stanowiska w tabeli pracownik2 powinny zosta� automatycznie zaktualizowane. 

ALTER TABLE pracownik3
ADD CONSTRAINT OND_stanowisko3
FOREIGN KEY (id_stanowisko)
REFERENCES stanowisko2(id_stanowisko)
ON DELETE SET NULL

ALTER TABLE pracownik3
ADD CONSTRAINT ONU_stanowisko2
FOREIGN KEY (id_stanowisko)
REFERENCES stanowisko2(id_stanowisko)
ON UPDATE CASCADE
