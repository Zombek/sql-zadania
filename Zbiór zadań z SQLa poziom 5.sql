--38.1 Napisa� wyzwalacz, kt�ry b�dzie wypisywa� na ekranie nazwiska i imiona usuni�tych pracownik�w. U�yj kursora. 
--Przetestuj utworzony wyzwalacz � usu� wszystkich pracownik�w. 

--38.2 Stw�rz tabel� o nazwie samochod_delete o identycznej strukturze jak tabela samoch�d. Napisz wyzwalacz, 
--kt�ry wszystkie usuni�te samochody z tabeli samoch�d wpisze do tabeli samochod_delete. 
--Przetestuj utworzony wyzwalacz � usu� z tabeli samoch�d samochody o identyfikatorach z przedzia�u od 1 do 4. 

--38.3 Napisa� wyzwalacz, kt�ry po podwy�ce pensji pracownikowi b�dzie zerowa� jego dodatek.  
--Przetestuj utworzony wyzwalacz � pracownikom o identyfikatorach r�wnych 1, 2 i 3 zwi�ksz pensj� o 200 z�.

--39.1 Utw�rz widok o nazwie ocena_klienta, kt�ry dla ka�dego klienta (id_klient, imie, nazwisko) wy�wietli informacj�, 
--czy to jest sta�y klient (czyli wypisze s�owo TAK lub NIE jako warto�� kolumny "staly_klient").  
--Przyjmijmy, �e sta�y klient to taki, kt�ry wypo�yczy� co najmniej dwa razy samoch�d. U�yj powy�szego widoku i 
--wypisz sta�ych klient�w sortuj�c wynik alfabetycznie po nazwisku i imieniu klienta. 

CREATE VIEW ocena_klienta 
	AS 
		SELECT k.id_klient, k.imie, k.nazwisko,   
		CASE WHEN COUNT(w.id_klient)>=2 THEN 'TAK' ELSE 'NIE' END  AS staly_klient FROM klient k 
		LEFT JOIN wypozyczenie w ON k.id_klient=w.id_klient
		GROUP BY k.id_klient, k.imie, k.nazwisko; 
		GO 
		SELECT * FROM ocena_klienta WHERE staly_klient='TAK' ORDER BY nazwisko, imie;  

--39.2 Utw�rz widok o nazwie pensja_pracownika, kt�ry dla ka�dego pracownika (id_pracownik, imie, nazwisko) wy�wietli informacj� 
--w nowej kolumnie o nazwie "zarobki" o rz�dzie jego zarobk�w: MA�O, �REDNIO, DU�O.Przyjmijmy, �e pracownik zarabia MA�O je�li 
--pensja jest poni�ej 1500 z�, zarabia �REDNIO je�li pensja jest od 1500 z� do 3000 z�, a zarabia du�o je�li pensja jest wi�ksza od 3000 z�. 
--U�yj powy�szego widoku do znalezienia pracownik�w zarabiaj�cych MA�O. 
	CREATE VIEW pensja_pracownika
	AS
		SELECT id_pracownik, imie, nazwisko,
		CASE WHEN pensja < 1500 THEN 'MA�O'
		WHEN pensja BETWEEN 1500 AND 3000 THEN '�REDNIO'
		ELSE 'DUZO' END AS zarobki
		FROM pracownik
	GO

	DROP VIEW pensja_pracownika

	SELECT * FROM pensja_pracownika WHERE zarobki = 'MA�O'

--39.3 Utw�rz widok o nazwie "pracownik_informacje", kt�ry wy�wietla o ka�dym pracowniku takie informacje jak:  
--a) id_pracownik, b) imie, c) nazwisko, d) kolumna wyliczeniowa "staz_pracy", czyli d�ugo�� zatrudnienia w latach,  
--e) kolumna wyliczeniowa "wypozyczenia", czyli ca�kowita ilo�� wypo�ycze� samochod�w, f) kolumna wyliczeniowa "dodatek" 
--z warto�ciami TAK/NIE/BRAK (TAK gdy dodatek>0, NIE gdy dodatek=0, BRAK w pozosta�ych przypadkach). 
--Przetestuj utworzony widok wyszukuj�c pracownik�w, dla kt�rych dodatek nie zosta� przyznany. 

CREATE VIEW pracownik_informacje
	AS
		SELECT p.id_pracownik, p.imie, p.nazwisko,
		DATEDIFF(YEAR, p.data_zatr, GETDATE()) as staz_pracy,
		COUNT(w.id_pracow_wyp) AS wypozyczenia,
		CASE WHEN p.dodatek > 0 THEN 'TAK'
			 WHEN p.dodatek	= 0 THEN 'NIE'
			 ELSE 'BRAK' END AS dodatek
		FROM pracownik p
		LEFT JOIN wypozyczenie w ON w.id_pracow_wyp = p.id_pracownik
		GROUP BY p.id_pracownik, p.imie, p.nazwisko, data_zatr, dodatek
	GO

SELECT * FROM pracownik_informacje
WHERE dodatek IN ('NIE', 'BRAK')


--40.1 Napisa� procedur� o nazwie "usun_widoki", kt�ra z bie��cej bazy danych usunie wszystkie widoki. 
--Przetestuj dzia�anie utworzonej procedury. Wsk 1: SELECT * FROM INFORMATION_SCHEMA.TABLES; Wsk 2: EXEC sp_executesql @zapytanie_sql 

--40.2 Napisa� procedur� o nazwie "usun_klucze_obce", kt�ra z bie��cej bazy danych usunie wszystkie klucze obce. 
--Przetestuj dzia�anie utworzonej procedury. Wsk 1: SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS; 

--40.3 Napisa� procedur� o nazwie "klient_dodaj_kolumne_rabat", kt�ra sprawdza czy w tabeli klient istnieje kolumna o nazwie "rabat", 
---je�li nie to dodaje j� do tabeli klient. Kolumna "rabat" powinna by� typu INT i przyjmowa� domy�lnie warto�� 0. 
--Wszyscy istniej�cy klienci w tabeli klient powinni mie� ustawion� warto�� rabatu na 0. Przetestuj dzia�anie procedury. 

--40.4 Napisa� procedur� o nazwie "baza_reset" posiadaj�c� jeden parametr, kt�ry jest nazw� bazy danych.Procedura ma usun�� zadan� baz� 
--danych (o ile taka istnieje) i utworzy� j� na nowo.  Przetestuj dzia�anie procedury. 

--40.5 Napisa� procedur� o nazwie "tabele_statystyka", kt�ra dla bie��cej bazy danych wy�wietli w formietabelarycznej informacj� o 
--wszystkich tabelach znajduj�cych si� w bie��cej bazie danych oraz ilo�ci rekord�w w ka�dej z tych tabel. 
--Przyjmij, �e kolumny uzyskanego zestawienia b�d� mie� nazwy: nazwa_tabeli, ilosc_rekordow. Przetestuj dzia�anie utworzonej procedury.